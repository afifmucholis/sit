<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Artikel | Tulisan Kita Semua</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" href="material.min.css">
    <link rel="stylesheet" href="styles2.css">
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-layout--fixed-header mdl-js-layout mdl-color--grey-100">
      <header class="demo-header mdl-layout__header mdl-layout__header--scroll  mdl-color-text--grey-800">
        <div class="mdl-layout__header-row">
         <?php
        echo "<a href='index.php' class='mdl-layout__tab '>Beranda</a>";
        include"koneksi.php";
        $ambildata="select id_kategori, nama from kategori";
        $hasilambil=mysql_query($ambildata);
        while($data=mysql_fetch_array($hasilambil)) {
          echo "<a href='kategori.php?id=$data[id_kategori]' class='mdl-layout__tab '>$data[nama]</a>";
        } 
        ?>
<!--           <span class="mdl-layout-title">Tulisan Kita Semua</span>
 -->          <div class="mdl-layout-spacer"></div>
        </div>
      </header>
      <div class="demo-ribbon"></div>
      <main class="demo-main mdl-layout__content">
        <div class="demo-container mdl-grid">
          <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
          <div class="demo-crumbs mdl-color-text--grey-500">
              <a href='index.php'>Tulisan Kita Semua</a> &gt; Daftar jadi penulis
            <h3>Pengen nulis artikel??? Daftar dulu yuk. . .</h3>
            </div>
             <form action="aksidaftar.php" method="POST">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                  <input type="text" class="mdl-textfield__input" id="nama" name="nama" required></textarea>
                  <label for="comment" class="mdl-textfield__label">Nama Lengkap</label>
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                  <input type="email" class="mdl-textfield__input" id="nama" name="email" required></textarea>
                  <label for="comment" class="mdl-textfield__label">E-mail</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                  <input type="number" class="mdl-textfield__input" id="nama" name="telpon" required></textarea>
                  <label for="comment" class="mdl-textfield__label">Telpon</label>
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                 <button class="mdl-button mdl-js-button mdl-js-ripple-effect" name="submit">
                  <i class="material-icons" role="presentation">check</i><span >Daftar</span>
                </button>
                </div>
              </form> 
              <p>
                Keterangan :<br> 
                Setelah anda mendaftar anda akan mendapatkan <i> <b> key</b></i> yang digunakan agar anda bisa mengirim artikel.
              </p>
                         
          </div>
        </div>
        <footer class="demo-footer mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <ul class="mdl-mini-footer--link-list">
              <li><a href="#">Help</a></li>
              <li><a href="#">Privacy and Terms</a></li>
              <li><a href="#">User Agreement</a></li>
            </ul>
          </div>
        </footer>
      </main>
    </div>
     </body>
     <script src="material.min.js"></script>
</html>
