<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Artikel | Tulisan Kita Semua</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" href="material.min.css">
    <link rel="stylesheet" href="styles2.css">
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-layout--fixed-header mdl-js-layout mdl-color--grey-100">
      <header class="demo-header mdl-layout__header mdl-layout__header--scroll  mdl-color-text--grey-800">
        <div class="mdl-layout__header-row">
         <?php
        echo "<a href='index.php' class='mdl-layout__tab is-active'>Beranda</a>";
        include"koneksi.php";
        $ambildata="select id_kategori, nama from kategori";
        $hasilambil=mysql_query($ambildata);
        while($data=mysql_fetch_array($hasilambil)) {
          echo "<a href='kategori.php?id=$data[id_kategori]' class='mdl-layout__tab is-active'>$data[nama]</a>";
        } 
        ?>
<!--           <span class="mdl-layout-title">Tulisan Kita Semua</span>
 -->          <div class="mdl-layout-spacer"></div>
        </div>
      </header>
      <div class="demo-ribbon"></div>
      <main class="demo-main mdl-layout__content">
        <div class="demo-container mdl-grid">
          <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
      <?php 
      if (isset($_GET['id'])) {
        $id_post = $_GET['id'];
        $cek = "select id_post from post where id_post = '$id_post' ";
        $hasilcek=mysql_query($cek);
        if ($hasilcek != null) { 
          $ambilpost="select post.id_post, post.judul, post.isi, author.nama_author as penulis, post.created, kategori.id_kategori, kategori.nama  from post join kategori using(id_kategori) join author using(id_author) where id_post = $id_post";
          $hasil=mysql_query($ambilpost);
          while($data=mysql_fetch_array($hasil)) { ?>
            <div class="demo-crumbs mdl-color-text--grey-500">
              <a href='index.php'>Tulisan Kita Semua</a> &gt; <a href="kategori.php?id=<?=$data['id_kategori']?>" > <?=$data['nama']?> </a>  &gt; <?=$data['judul']?> 
            <h3><?=$data['judul']?></h3>
            </div>
              <p>
                <?=$data['isi']?>
              </p>
              <div class="demo-crumbs mdl-color-text--grey-500">
              Author : <?=$data['penulis']?>
            </div>
            <div class="demo-crumbs mdl-color-text--grey-500">
              <?=$data['created']?>
            </div>
        <?php 
      }
      }else{
          echo "404 Not Found!!!";
        }
      }else{
        echo "404 Not Found!!!";
      }
      ?>    
          </div>
        </div>
        <footer class="demo-footer mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <ul class="mdl-mini-footer--link-list">
              <li><a href="#">Help</a></li>
              <li><a href="#">Privacy and Terms</a></li>
              <li><a href="#">User Agreement</a></li>
            </ul>
          </div>
        </footer>
      </main>
    </div>
     </body>
</html>
